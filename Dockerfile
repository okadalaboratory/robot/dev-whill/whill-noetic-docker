# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM osrf/ros:noetic-desktop-full-focal

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="December 31, 2023"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive 

# Timezone, Launguage設定
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe
# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo

RUN apt-get update && apt-get install -y \
    wget build-essential gcc git vim  python-is-python3 \
    lsb-release iproute2 gnupg gnupg2 gnupg1 \
    ca-certificates language-pack-ja-base  \
    language-pack-ja locales fonts-takao \
    terminator \
    python3-pip ros-noetic-serial usbutils \
    && rm -rf /var/lib/apt/lists/*


# Create an overlay Catkin workspace
RUN source /opt/ros/noetic/setup.bash \
 && mkdir -p /overlay_ws/src \
 && cd /overlay_ws/src \ 
 && catkin_init_workspace \ 
 && git clone https://github.com/WHILL/ros_whill.git \
 && git clone https://gitlab.com/okadalaboratory/robot/dev-whill/whill-noetic.git  \
 && cd /overlay_ws \
 && rosdep update \
 && rosdep install --from-paths src --ignore-src -r -y \
 && catkin_make

COPY assets/ros_entrypoint.sh /tmp
RUN chmod a+x /tmp/ros_entrypoint.sh

ARG USER_NAME=whill
ARG GROUP_NAME=whill
ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID $USER_NAME
USER $USERNAME
WORKDIR /home/$USER_NAME/

RUN echo "source /opt/ros/noetic/setup.bash" >> /home/$USER_NAME/.bashrc
RUN echo "source /overlay_ws/devel/setup.bash" >> /home/$USER_NAME/.bashrc
RUN rosdep update

ENTRYPOINT [ "/tmp/ros_entrypoint.sh" ]
# CMD ["/bin/bash"]
CMD ["terminator"]

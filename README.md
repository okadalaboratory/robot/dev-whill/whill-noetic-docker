# WHILL Noetic Docker
DockerでWHILL の制御システムを運用する

## To Do
31 Dec. 2023
大幅改修中

## 準備

## Dockerコンテナで音声を再生する
Dockerコンテナ側では音を再生できないので、以下の手段でDockerコンテナ側から生成された音声をホストPCで再生します。
Docker コンテナで音声を再生するために、ホストコンピュータのPulseaudioを使います。


下記のコマンドでホストPCの/tmp/pulseaudio.socketを生成する。同じ名前のディレクトリが存在する場合は、そのディレクトリを削除する。

```
ホストPC $ ls -al 
合計 72
drwxrwxr-x 18 roboworks roboworks 4096 12月 29 15:44 .
drwxr-x--- 29 roboworks roboworks 4096 12月 29 10:33 ..
drwxrwxr-x  4 roboworks roboworks 4096 12月 25 10:53 pulseaudio.socket
...
...
ホストPC $ sudo rm -r /tmp/pulseaudio.socket
ホストPC $ pacmd load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket
```

/tmp/pulseaudio.client.confを作成して、下記の通り編集します
```
default-server = unix:/tmp/pulseaudio.socket
# Prevent a server running in the container
autospawn = no
daemon-binary = /bin/true
# Prevent the use of shared memory
enable-shm = false
```
あるいは
ホストコンピュータの /etc/pulse/default.pa　に、下記 socket=...の記述を追加します。
```
load-module module-native-protocol-unix
load-module module-native-protocol-unix socket=/tmp/.pulseaudio.socket
```
再起動してください。

ホストコンピュータの/tmp/pulseaudio.socketをコンテナの/tmp/pulseaudio.socketにマッピングします。
詳細はdocker-compose.ymlの設定を確認してください。

## インストール
```
cd ~
git clone https://gitlab.com/okadalaboratory/text-to-speech/whill-docker-noetic.git
```

## Dockerイメージの作成
WHILL制御システムのDockerイメージはCPU版が用意されています。
```
cd ~/whill-docker-noetic
docker compose build
```

## Dockerコンテナの起動
```
$ ocker compose run
[+] Running 3/0
 ✔ Container whill-noetic-docker-ros-master-1  Cr...                                              0.0s 
 ✔ Container voicevox                          Created                                            0.0s 
 ✔ Container whill                             Created                                            0.0s 
Attaching to voicevox, whill, whill-noetic-docker-ros-master-1
whill                             | whill@1520256552e2:~$ roslaunch ros_whill ros_whill.launch
voicevox                          | voicevox@voicevox:~$ roslaunch voicevox_ros voicevox_sounddevice.launch speaker:=1
...
...
whill                             | started roslaunch server http://whill:36519/
whill                             | 
whill                             | SUMMARY
whill                             | ========
whill                             | 
whill                             | PARAMETERS
whill                             |  * /robot_description: <?xml version="1....
whill                             |  * /rosdistro: noetic
whill                             |  * /rosversion: 1.16.0
whill                             |  * /whill/init_speed/backward/acc: 0.34
whill                             |  * /whill/init_speed/backward/dec: 0.89
whill                             |  * /whill/init_speed/backward/speed: 0.557
whill                             |  * /whill/init_speed/forward/acc: 0.23
whill                             |  * /whill/init_speed/forward/dec: 1.25
whill                             |  * /whill/init_speed/forward/speed: 0.567
whill                             |  * /whill/init_speed/turn/acc: 0.79
whill                             |  * /whill/init_speed/turn/dec: 1.8
whill                             |  * /whill/init_speed/turn/speed: 0.8
whill                             |  * /whill/keep_connected: True
whill                             |  * /whill/publish_tf: True
whill                             |  * /whill/serialport: /dev/ttyUSB0
whill                             | 
whill                             | NODES
whill                             |   /
whill                             |     robot_state_publisher (robot_state_publisher/robot_state_publisher)
whill                             |     whill (ros_whill/ros_whill)
whill                             | 
whill                             | ROS_MASTER_URI=http://ros-master:11311
whill                             | 
...
...
voicevox                          | started roslaunch server http://voicevox:44209/
voicevox                          | 
voicevox                          | SUMMARY
voicevox                          | ========
voicevox                          | 
voicevox                          | PARAMETERS
voicevox                          |  * /rosdistro: noetic
voicevox                          |  * /rosversion: 1.16.0
voicevox                          |  * /voicevox/opening_msg: <...>
voicevox                          |  * /voicevox/speaker_id: 1
voicevox                          | 
voicevox                          | NODES
voicevox                          |   /
voicevox                          |     voicevox (voicevox_ros/voicevox_sounddevice.py)
voicevox                          | 
voicevox                          | ROS_MASTER_URI=http://ros-master:11311
voicevox                          | 
voicevox                          | process[voicevox-1]: started with pid [61]
...
...
whill                             | whill@1520256552e2:~$ roslaunch ros_whill ros_whill.launch
whill                             | ... logging to /home/whill/.ros/log/23bfe0d8-a618-11ee-8fcb-0242ac1a0002/roslaunch-1520256552e2-53.log
whill                             | Checking log directory for disk usage. This may take a while.
whill                             | Press Ctrl-C to interrupt
whill                             | Done checking log file disk usage. Usage is <1GB.
whill                             | 
voicevox                          | [INFO] [1703833311.580274]: say opening Message
whill                             | started roslaunch server http://whill:35205/
whill                             | 
whill                             | SUMMARY
whill                             | ========
whill                             | 
whill                             | PARAMETERS
whill                             |  * /robot_description: <?xml version="1....
whill                             |  * /rosdistro: noetic
whill                             |  * /rosversion: 1.16.0
whill                             |  * /whill/init_speed/backward/acc: 0.34
whill                             |  * /whill/init_speed/backward/dec: 0.89
whill                             |  * /whill/init_speed/backward/speed: 0.557
whill                             |  * /whill/init_speed/forward/acc: 0.23
whill                             |  * /whill/init_speed/forward/dec: 1.25
whill                             |  * /whill/init_speed/forward/speed: 0.567
whill                             |  * /whill/init_speed/turn/acc: 0.79
whill                             |  * /whill/init_speed/turn/dec: 1.8
whill                             |  * /whill/init_speed/turn/speed: 0.8
whill                             |  * /whill/keep_connected: True
whill                             |  * /whill/publish_tf: True
whill                             |  * /whill/serialport: /dev/ttyUSB0
whill                             | 
whill                             | NODES
whill                             |   /
whill                             |     robot_state_publisher (robot_state_publisher/robot_state_publisher)
whill                             |     whill (ros_whill/ros_whill)
whill                             | 
whill                             | ROS_MASTER_URI=http://ros-master:11311
whill                             | 
whill                             | process[robot_state_publisher-1]: started with pid [63]
whill                             | process[whill-2]: started with pid [64]
...
...
```
コンテナ起動の音声が流れれば成功です。

## Whillを動かしてみる